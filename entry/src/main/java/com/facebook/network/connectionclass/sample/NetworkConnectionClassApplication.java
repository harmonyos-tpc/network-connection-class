package com.facebook.network.connectionclass.sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * NetworkConnectionClassApplication
 *
 * @since 2021-02-26
 */
public class NetworkConnectionClassApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
